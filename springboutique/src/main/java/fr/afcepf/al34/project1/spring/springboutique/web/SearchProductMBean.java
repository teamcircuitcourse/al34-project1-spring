package fr.afcepf.al34.project1.spring.springboutique.web;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import fr.afcepf.al34.project1.spring.springboutique.business.CategoryService;
import fr.afcepf.al34.project1.spring.springboutique.business.ProductService;
import fr.afcepf.al34.project1.spring.springboutique.entity.Category;
import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ManagedBean
@Getter @Setter @NoArgsConstructor
@SessionScoped
public class SearchProductMBean {

	private Product product;
	private List<Product> products;
	private List<Category> categories;
	
	private Integer categoryId;
	@Inject
	private ProductService productService;
	@Inject
	private CategoryService categoryService;
	
public String getAll() {
	this.products = productService.getAllProducts();
	return "resultSearch.xhtml?faces-redirect=true";
}
public String searchByCustomerGender (Integer customerGenderId) {
	this.products = productService.findWithGender(customerGenderId);
	return "resultSearch.xhtml?faces-redirect=true";
	
}
public String searchByCategoryName (Integer categoryId) {
	this.products = productService.findWithCategory(categoryId);
	return "resultSearch.xhtml?faces-redirect=true";
	
}
public String searchByCustomerGenderAndCategory (Integer customerGenderId, Integer categoryId) {
	this.products = productService.findWithCustomerGenderAndCategory(customerGenderId,categoryId);
	return "resultSearch.xhtml?faces-redirect=true";
	
}
public String searchByCustomerGenderAndSport (Integer customerGenderId, Integer sportId) {
	this.products = productService.findWithCustomerGenderAndSport(customerGenderId,sportId);
	return "resultSearch.xhtml?faces-redirect=true";
	
}
}
