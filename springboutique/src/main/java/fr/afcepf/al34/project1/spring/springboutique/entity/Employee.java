package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@DiscriminatorValue("Employee")
public class Employee extends User implements Serializable{
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "start_date")
	private LocalDate startDate;

	@Column(name = "end_date")
    private LocalDate endDate;

    @OneToMany (mappedBy = "employee")
    private List<Grade> grades = new ArrayList<Grade> ();

    @OneToMany
    private List<Promotion> promotions = new ArrayList<Promotion> ();

    @Column(name = "employee_products")
    @OneToMany(mappedBy = "product")
    private List<EmployeeProduct> employeeProducts = new ArrayList<EmployeeProduct> ();

	public Employee(LocalDate startDate, LocalDate endDate, List<Grade> grades, List<Promotion> promotions,
			List<EmployeeProduct> employeeProducts) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.grades = grades;
		this.promotions = promotions;
		this.employeeProducts = employeeProducts;
	}

}
