package fr.afcepf.al34.project1.spring.springboutique.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.project1.spring.springboutique.entity.Inventory;


public interface InventoryDao extends CrudRepository<Inventory, Integer> {
	Inventory findMinStockById(Integer idInventory);
	Inventory findQuantityById(Integer idInventory);

}
