package fr.afcepf.al34.project1.spring.springboutique.init;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import fr.afcepf.al34.project1.spring.springboutique.business.AuthenticationManager;
import fr.afcepf.al34.project1.spring.springboutique.business.CartService;
import fr.afcepf.al34.project1.spring.springboutique.business.CategoryService;
import fr.afcepf.al34.project1.spring.springboutique.business.ClientService;
import fr.afcepf.al34.project1.spring.springboutique.business.CredentialsService;
import fr.afcepf.al34.project1.spring.springboutique.business.CustomerGenderService;
import fr.afcepf.al34.project1.spring.springboutique.business.ProductService;
import fr.afcepf.al34.project1.spring.springboutique.business.PromotionService;
import fr.afcepf.al34.project1.spring.springboutique.business.SportProductService;
import fr.afcepf.al34.project1.spring.springboutique.business.InventoryService;
import fr.afcepf.al34.project1.spring.springboutique.business.UserService;
import fr.afcepf.al34.project1.spring.springboutique.entity.BankCard;
import fr.afcepf.al34.project1.spring.springboutique.entity.Batch;
import fr.afcepf.al34.project1.spring.springboutique.entity.CardType;
import fr.afcepf.al34.project1.spring.springboutique.entity.Type;
import fr.afcepf.al34.project1.spring.springboutique.entity.Cart;
import fr.afcepf.al34.project1.spring.springboutique.entity.Category;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Composition;
import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;
import fr.afcepf.al34.project1.spring.springboutique.entity.CustomerAge;
import fr.afcepf.al34.project1.spring.springboutique.entity.CustomerGender;
import fr.afcepf.al34.project1.spring.springboutique.entity.Employee;
import fr.afcepf.al34.project1.spring.springboutique.entity.Gender;
import fr.afcepf.al34.project1.spring.springboutique.entity.Inventory;
import fr.afcepf.al34.project1.spring.springboutique.entity.Photo;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.entity.Promotion;
import fr.afcepf.al34.project1.spring.springboutique.entity.Sport;
import fr.afcepf.al34.project1.spring.springboutique.entity.SportProduct;

@Profile("initData")
@Component
public class InitDataSet {

	@Autowired
	ClientService clientService;

	@Autowired
	CredentialsService credentialsService;

	@Autowired
	UserService userService;

	@Autowired
	ProductService productService;
	
	@Autowired
	PromotionService promotionService;

	@Autowired
	SportProductService sportProductService;
	
	@Autowired
	CartService cartService;
	
	@Autowired
	InventoryService inventoryService;
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	CustomerGenderService customerGenderService;

	@PostConstruct
	public void initData() {
		Gender monsieur = userService.saveInBase(new Gender("Monsieur"));
		Gender madame = userService.saveInBase(new Gender("Madame"));
		
		Type type0 = inventoryService.saveInBase(new Type("XXL", "Bleu"));
		Type type1 = inventoryService.saveInBase(new Type("XL", "Jaune"));
		Type type2 = inventoryService.saveInBase(new Type("L", "Mauve"));
		Type type3 = inventoryService.saveInBase(new Type("M", "Noir"));
		Type type4 = inventoryService.saveInBase(new Type("S", "Blanc"));
		Type type5 = inventoryService.saveInBase(new Type("14 ans", "Cyan"));
		Type type6 = inventoryService.saveInBase(new Type("12 ans", "Gris"));
		Type type7 = inventoryService.saveInBase(new Type("10 ans", "Rose"));
		Type type8 = inventoryService.saveInBase(new Type("8 ans", "Bordeau"));
		Type type9 = inventoryService.saveInBase(new Type("38", "Rouge"));
		Type type10 = inventoryService.saveInBase(new Type("39", "Marron"));
		Type type11 = inventoryService.saveInBase(new Type("40", "Orange"));
		Type type12 = inventoryService.saveInBase(new Type("41", "Vert"));
		Type type13 = inventoryService.saveInBase(new Type("42", "Taupe"));
		Type type14 = inventoryService.saveInBase(new Type("43", "Vert Pomme"));
		Type type15= inventoryService.saveInBase(new Type("44", "Bleu Marine"));
		Type type16 = inventoryService.saveInBase(new Type("45", "Cuivre"));
		
		CustomerGender man = customerGenderService.saveInBase(new CustomerGender("Homme"));
		CustomerGender woman = customerGenderService.saveInBase(new CustomerGender("Femme"));
		
		CustomerAge kids = categoryService.saveInBase(new CustomerAge("Kids"));
		CustomerAge adults = categoryService.saveInBase(new CustomerAge("Adulte"));
		CustomerAge senior = categoryService.saveInBase(new CustomerAge("Senior"));
		
		Sport ski = categoryService.saveInBase(new Sport("Ski"));
		Sport surf = categoryService.saveInBase(new Sport("Surf"));
		Sport rando = categoryService.saveInBase(new Sport("Rando"));
		Sport trek = categoryService.saveInBase(new Sport("Trek"));
		Sport fitness = categoryService.saveInBase(new Sport("Fitness"));
		Sport xtrem = categoryService.saveInBase(new Sport("X-Trem"));
		
		Category tshirt = categoryService.saveInBase(new Category("T-shirt"));
		Category pull = categoryService.saveInBase(new Category("Pull"));
		Category legging = categoryService.saveInBase(new Category("Legging"));
		Category jogging = categoryService.saveInBase(new Category("Jogging"));
		Category shoes = categoryService.saveInBase(new Category("Chaussure"));
		Category equipment = categoryService.saveInBase(new Category("Equipement"));
		Category access = categoryService.saveInBase(new Category("Accessoire"));
		
		Composition coton = categoryService.saveInBase(new Composition("Coton",true));
		Composition polyester = categoryService.saveInBase(new Composition("Polyester",false));
		Composition polyamide = categoryService.saveInBase(new Composition("Polyamide",false));
		Composition elasthanne = categoryService.saveInBase(new Composition("Élasthanne",false));
		Composition lycra = categoryService.saveInBase(new Composition("Lycra",false));
		Composition duvet = categoryService.saveInBase(new Composition("Duvet",true));
		Composition plume = categoryService.saveInBase(new Composition("Plume",true));
		Composition goreTex = categoryService.saveInBase(new Composition("Gore-Tex",false));
		
		Photo photo = productService.saveInBase(new Photo("photoTest", "bra.jpg"));
		Photo photo2 = productService.saveInBase(new Photo("photoTest2", "bigBra.jpg"));
		List<Photo> photos = new ArrayList();
		photos.add(photo);
		photos.add(photo2);
		Product productTest = new Product("Brassière", "Brassière de Sport pour femme.", 10);
		productTest.setPhotos(photos);
		productTest.setCategory(tshirt);
		productTest.setCustomerGender(woman);
		productTest = productService.saveInBase(productTest);
		List<SportProduct> sp= new ArrayList<SportProduct>();
		SportProduct newSp =sportProductService.saveInBase(new SportProduct(fitness, productTest));
		sp.add(newSp);
		productTest.setSportProducts(sp);
		productService.saveInBase(productTest);
		
		LocalDateTime today =  LocalDateTime.now();     //aujourd'hui
		LocalDateTime later = LocalDateTime.now().plusWeeks(2);     //dans 2 semaines
		Promotion promo = promotionService.saveInBase(new Promotion("-15%", 15, today, later));
		
		CardType cardTypeA = cartService.saveInBase(new CardType("mastercard"));
		CardType cardTypeB = cartService.saveInBase(new CardType("visa"));
		
		for(int i = 1; i<11 ; i++) {
			CardType cardType = new CardType();
			Gender gender = new Gender();
			CustomerGender customerGender = new CustomerGender();
			Credentials cred = new Credentials("login" + i);

			try {
				AuthenticationManager.initializeCredentials("password" + i , cred);
			} catch (Exception e) {
				e.printStackTrace();
			}
			credentialsService.saveInBase(cred);
			
			//	Employee employee = new Employee(LocalDate.now(), LocalDate.now();
			//TODO employee

			Product p = productService.saveInBase(new Product("Produit" + i, "super produit" + i*i, i*12.3));
			photos = new ArrayList();
			

			
			for (int j = 1 ; j < 5; j++) {
				Photo prodPhoto = productService.saveInBase(new Photo("Produit" + i, "photo"+i+j+".jpg"));
				photos.add(prodPhoto);
			}
			p.setPhotos(photos);
			productService.saveInBase(p);
			
			if(i%2==0) {
			p.setPromotion(promo);
			cardType = cardTypeA;
			gender = madame;
			customerGender = woman;
			}else {
				cardType = cardTypeB;
				gender = monsieur;
				customerGender = man;
			}
			
			
			Client client = new Client("Firstname" + i, "Lastname" + i,
					"user" + i + "al34.afcepf.com", "063075189" + i, cred, madame);
			BankCard bankCard = cartService.saveInBase(new BankCard("Bank" + i,
					i+"536497135"+i+2*i+"3"+i, LocalDate.now().plusYears(2), i+6+3*i,
					client.getFirstName(), cardType));
			client.getBankCard().add(bankCard);
			
			Inventory inventory = inventoryService.saveInBase(new Inventory(2*i, (2*i)/3, type1));
			List<Inventory> inventories = new ArrayList<Inventory>();
			inventories.add(inventory);

			p.setCustomerGender(customerGender);
			p.setInventories(inventories);
			p = productService.saveInBase(p);

			Cart cart = cartService.saveInBase(new Cart());
			Batch batch = cartService.saveInBase(new Batch(i, LocalDate.now(), p, cart));
			List<Batch> batches = new ArrayList<Batch>();
			batches.add(batch);

			cart.setBatches(batches);
			client = clientService.saveInBase(client);
			cart.setClient(client);
			cart = cartService.saveInBase(cart);
			client.setCart(cart);
			clientService.saveInBase(client);
		}
	}
}
