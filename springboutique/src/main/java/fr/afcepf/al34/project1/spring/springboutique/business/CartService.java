package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.entity.BankCard;
import fr.afcepf.al34.project1.spring.springboutique.entity.Batch;
import fr.afcepf.al34.project1.spring.springboutique.entity.CardType;
import fr.afcepf.al34.project1.spring.springboutique.entity.Cart;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;


public interface CartService {

	Cart saveInBase(Cart cart);
	Cart findWithClient(Client client);
	Batch saveInBase(Batch batch);
	List<Product> findWithCart(Cart cart);
	void removeFromCart(Batch batch);
	CardType saveInBase(CardType type);
	BankCard saveInBase(BankCard cb);
	void removeCart(Cart cart);
	void removeBatch(Batch batch);
}
