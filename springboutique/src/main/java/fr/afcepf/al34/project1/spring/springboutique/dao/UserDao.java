package fr.afcepf.al34.project1.spring.springboutique.dao;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al34.project1.spring.springboutique.entity.User;

public interface UserDao extends CrudRepository<User, Integer> {

}
