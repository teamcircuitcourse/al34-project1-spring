package fr.afcepf.al34.project1.spring.springboutique.web;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import fr.afcepf.al34.project1.spring.springboutique.business.ProductService;
import fr.afcepf.al34.project1.spring.springboutique.business.PromotionService;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.entity.Promotion;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ManagedBean
@SessionScoped
@Getter @Setter @NoArgsConstructor
public class PromotionMBean {

	private Promotion promo;
	
	private List<Promotion> promotions;
	
	private List<Product> products;
	
	private List<Product> productsToPromote;
	
	@Inject
	PromotionService promotionService;
	
	@Inject
	ProductService productService;
	
	@PostConstruct
	public void init() {
		System.out.println("init bean");
		products = promotionService.findWithPromotion();
		productsToPromote = productService.getAllProducts();
		promo = new Promotion();
	}
	
	public String doAddPromo() {
		promo.setProducts(productsToPromote);
		promotionService.saveInBase(promo);
		for (Product product : productsToPromote) {
			product.setPromotion(promo);
			productService.saveInBase(product);
		}
		return "clientMain.xhtml?faces-redirect=true";
	}
}
