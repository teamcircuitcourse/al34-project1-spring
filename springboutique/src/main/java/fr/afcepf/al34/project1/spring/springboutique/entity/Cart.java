package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "cart")
public class Cart implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

	@Column(name = "order_number")
    private String orderNumber;

	@Column(name = "is_paid")
    private boolean isPaid;

	@Column(name = "is_delivered")
    private boolean isDelivered;

    private LocalDateTime date;
    
    @OneToOne
    @JoinColumn(referencedColumnName = "id")
    private Client client;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "cart")
    private List<Batch> batches;
    
    public Cart (List<Batch> batches) {
    	this.isPaid = false;
    	this.isDelivered = false;
    	this.batches = batches;
    }


}
