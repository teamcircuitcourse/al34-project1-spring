package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.dao.BatchDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.CartDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.InventoryDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.TypeDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.Batch;
import fr.afcepf.al34.project1.spring.springboutique.entity.Cart;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Gender;
import fr.afcepf.al34.project1.spring.springboutique.entity.Inventory;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.entity.Type;

@Service
@Transactional
public class InventoryServiceImpl implements InventoryService {

	@Autowired
	InventoryDao inventoryDao;
	
	@Autowired
	TypeDao typeDao;
		
	@Override
	public Inventory saveInBase(Inventory inventory) {
		return inventoryDao.save(inventory);
	}

	@Override
	public List<Type> getAllTypes() {
			return (List<Type>) typeDao.findAll();
		}

	@Override
	public Type saveInBase(Type type) {
		return typeDao.save(type);
	}

	@Override
	public List<String> getAllColors() {
		return (List<String>)typeDao.findAllColors();
	}

	@Override
	public List<String> getAllSizes() {
		return (List<String>)typeDao.findAllSizes();
	}

	@Override
	public Inventory getTotalMinStock(Integer idInventory) {
		return inventoryDao.findMinStockById(idInventory);
	}

	@Override
	public Inventory getTotalQuantity(Integer idInventory) {
		return inventoryDao.findQuantityById(idInventory);
	}

//	@Override
//	public void increaseMinStock(int minStock) {
//		minStock++;
//		
//		
//	}
//
//	@Override
//	public void decreaseMinStock(int minStock) {
//		minStock--;
//		
//	}


//	@Override
//	public Inventory findWithProduct(Product product) {
//		return inventoryDao.findByProduct(product);
//	}

}